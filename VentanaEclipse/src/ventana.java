import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JToolBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import java.awt.Label;
import javax.swing.JList;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.TextField;
import java.awt.ScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.AbstractListModel;
import javax.swing.JTable;
import javax.swing.Box;

public class ventana {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ventana window = new ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ventana() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(12, 0, 190, 24);
		frame.getContentPane().add(toolBar);
		
		JButton btnNewButton = new JButton("En vivo");
		toolBar.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Estadisticas");
		toolBar.add(btnNewButton_1);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 30, 679, 282);
		frame.getContentPane().add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Conexiones", null, panel_1, null);
		panel_1.setLayout(null);
		
		JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("eth0") {
				{
					DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("local");
						node_1.add(new DefaultMutableTreeNode("192.168.1.1"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("91.45.65.0/24");
						node_1.add(new DefaultMutableTreeNode("91.45.65.3"));
						node_1.add(new DefaultMutableTreeNode("91.45.65.8"));
						node_1.add(new DefaultMutableTreeNode("91.45.65.9"));
						node_1.add(new DefaultMutableTreeNode("91.45.65.10"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("76.83.5.0/24");
						node_1.add(new DefaultMutableTreeNode("76.83.5.1"));
						node_1.add(new DefaultMutableTreeNode("76.83.5.2"));
						node_1.add(new DefaultMutableTreeNode("76.83.5.3"));
						node_1.add(new DefaultMutableTreeNode("76.83.5.4"));
					add(node_1);
				}
			}
		));
		tree.setBounds(52, 12, 150, 231);
		panel_1.add(tree);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Recursos", null, panel_2, null);
		panel_2.setLayout(null);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(134, 33, 148, 14);
		panel_2.add(progressBar);
		
		Label label = new Label("CPU");
		label.setBounds(60, 33, 68, 21);
		panel_2.add(label);
		
		JProgressBar progressBar_1 = new JProgressBar();
		progressBar_1.setBounds(134, 72, 148, 14);
		panel_2.add(progressBar_1);
		
		JProgressBar progressBar_2 = new JProgressBar();
		progressBar_2.setBounds(134, 119, 148, 14);
		panel_2.add(progressBar_2);
		
		JScrollBar scrollBar = new JScrollBar();
		scrollBar.setBounds(420, -3, 17, 221);
		panel_2.add(scrollBar);
		
		Label label_1 = new Label("RAM");
		label_1.setBounds(60, 72, 68, 21);
		panel_2.add(label_1);
		
		Component horizontalGlue_1 = Box.createHorizontalGlue();
		horizontalGlue_1.setBounds(23, 72, 385, -19);
		panel_2.add(horizontalGlue_1);
		
		Label label_2 = new Label("I/O");
		label_2.setBounds(60, 119, 68, 21);
		panel_2.add(label_2);
		
		Component horizontalGlue = Box.createHorizontalGlue();
		horizontalGlue.setBounds(26, 103, 382, 10);
		panel_2.add(horizontalGlue);
		
		JPanel panel = new JPanel();
		tabbedPane.addTab("Temperatura", null, panel, null);
		panel.setLayout(null);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"CPU0", "CPU1", "CPU2", "CPU3"}));
		comboBox.setBounds(43, 31, 79, 29);
		panel.add(comboBox);
		
		TextField textField = new TextField();
		textField.setEditable(false);
		textField.setText("36 C");
		textField.setBounds(155, 31, 37, 19);
		panel.add(textField);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"AICP_0", "AICP_2", "VRM", "CHP_0", "CHP_1", "CHP_2"}));
		comboBox_1.setBounds(43, 72, 79, 24);
		panel.add(comboBox_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"HDD0", "HDD1", "HDD2", "HDD3"}));
		comboBox_2.setBounds(43, 115, 79, 24);
		panel.add(comboBox_2);
		
		TextField textField_1 = new TextField();
		textField_1.setEditable(false);
		textField_1.setText("54 C");
		textField_1.setBounds(155, 72, 37, 19);
		panel.add(textField_1);
		
		JList list = new JList();
		list.setBounds(267, 46, 115, 77);
		panel.add(list);
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"CPU", "Motherboard", "Storage"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		
		TextField textField_2 = new TextField();
		textField_2.setEditable(false);
		textField_2.setText("41 C");
		textField_2.setBounds(155, 120, 37, 19);
		panel.add(textField_2);
		
		Component verticalStrut = Box.createVerticalStrut(20);
		verticalStrut.setBounds(231, 12, 12, 214);
		panel.add(verticalStrut);
	}
	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}
			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}
}
