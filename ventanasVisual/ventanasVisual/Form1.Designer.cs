﻿namespace ventanasVisual
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("192.168.1.1");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("local", new System.Windows.Forms.TreeNode[] {
            treeNode1});
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("91.45.63.3");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("91.45.63.8");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("91.45.63.9");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("91.45.63.10");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("91.45.65.0/24", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6});
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("76.83.5.1");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("76.83.5.2");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("76.83.5.3");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("76.83.5.4");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("76.83.5.0/24", new System.Windows.Forms.TreeNode[] {
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11});
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("eth0", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode7,
            treeNode12});
            this.cont = new System.Windows.Forms.TabControl();
            this.Conexiones = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.progressBar2 = new System.Windows.Forms.ProgressBar();
            this.progressBar3 = new System.Windows.Forms.ProgressBar();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.cont.SuspendLayout();
            this.Conexiones.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // cont
            // 
            this.cont.Controls.Add(this.Conexiones);
            this.cont.Controls.Add(this.tabPage2);
            this.cont.Controls.Add(this.tabPage3);
            this.cont.Location = new System.Drawing.Point(1, 26);
            this.cont.Name = "cont";
            this.cont.SelectedIndex = 0;
            this.cont.Size = new System.Drawing.Size(799, 427);
            this.cont.TabIndex = 0;
            // 
            // Conexiones
            // 
            this.Conexiones.Controls.Add(this.treeView1);
            this.Conexiones.Location = new System.Drawing.Point(4, 22);
            this.Conexiones.Name = "Conexiones";
            this.Conexiones.Padding = new System.Windows.Forms.Padding(3);
            this.Conexiones.Size = new System.Drawing.Size(791, 401);
            this.Conexiones.TabIndex = 0;
            this.Conexiones.Text = "Conexiones";
            this.Conexiones.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.vScrollBar1);
            this.tabPage2.Controls.Add(this.textBox3);
            this.tabPage2.Controls.Add(this.textBox2);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.progressBar3);
            this.tabPage2.Controls.Add(this.progressBar2);
            this.tabPage2.Controls.Add(this.progressBar1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(791, 401);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Recursos";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.toolStripSeparator1,
            this.toolStripTextBox2});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(800, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripTextBox1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTextBox1.Image")));
            this.toolStripTextBox1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(49, 22);
            this.toolStripTextBox1.Text = "En vivo";
            this.toolStripTextBox1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.toolStripTextBox1.Click += new System.EventHandler(this.toolStripTextBox1_Click);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripTextBox2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripTextBox2.Image")));
            this.toolStripTextBox2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(71, 22);
            this.toolStripTextBox2.Text = "Estadisticas";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.listBox1);
            this.tabPage3.Controls.Add(this.textBox6);
            this.tabPage3.Controls.Add(this.textBox5);
            this.tabPage3.Controls.Add(this.textBox4);
            this.tabPage3.Controls.Add(this.comboBox3);
            this.tabPage3.Controls.Add(this.comboBox2);
            this.tabPage3.Controls.Add(this.comboBox1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(791, 401);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Temperatura";
            this.tabPage3.UseVisualStyleBackColor = true;
            this.tabPage3.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // treeView1
            // 
            this.treeView1.DataBindings.Add(new System.Windows.Forms.Binding("Name", global::ventanasVisual.Properties.Settings.Default, "eth0", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.treeView1.Location = new System.Drawing.Point(211, 42);
            this.treeView1.Name = global::ventanasVisual.Properties.Settings.Default.eth0;
            treeNode1.Name = "Nodo4";
            treeNode1.Text = "192.168.1.1";
            treeNode2.Name = "Nodo1";
            treeNode2.Text = "local";
            treeNode3.Name = "Nodo5";
            treeNode3.Text = "91.45.63.3";
            treeNode4.Name = "Nodo6";
            treeNode4.Text = "91.45.63.8";
            treeNode5.Name = "Nodo7";
            treeNode5.Text = "91.45.63.9";
            treeNode6.Name = "Nodo9";
            treeNode6.Text = "91.45.63.10";
            treeNode7.Name = "Nodo2";
            treeNode7.Text = "91.45.65.0/24";
            treeNode8.Name = "Nodo10";
            treeNode8.Text = "76.83.5.1";
            treeNode9.Name = "Nodo11";
            treeNode9.Text = "76.83.5.2";
            treeNode10.Name = "Nodo12";
            treeNode10.Text = "76.83.5.3";
            treeNode11.Name = "Nodo13";
            treeNode11.Text = "76.83.5.4";
            treeNode12.Checked = true;
            treeNode12.Name = "Nodo3";
            treeNode12.Text = "76.83.5.0/24";
            treeNode13.Name = "Nodo0";
            treeNode13.Text = "eth0";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode13});
            this.treeView1.Size = new System.Drawing.Size(252, 246);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(105, 66);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(194, 16);
            this.progressBar1.TabIndex = 0;
            // 
            // progressBar2
            // 
            this.progressBar2.Location = new System.Drawing.Point(105, 119);
            this.progressBar2.Name = "progressBar2";
            this.progressBar2.Size = new System.Drawing.Size(193, 15);
            this.progressBar2.TabIndex = 1;
            // 
            // progressBar3
            // 
            this.progressBar3.Location = new System.Drawing.Point(105, 172);
            this.progressBar3.Name = "progressBar3";
            this.progressBar3.Size = new System.Drawing.Size(193, 16);
            this.progressBar3.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(319, 62);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(30, 20);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "CPU";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Enabled = false;
            this.textBox2.Location = new System.Drawing.Point(318, 114);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(31, 20);
            this.textBox2.TabIndex = 4;
            this.textBox2.Text = "RAM";
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(319, 168);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(31, 20);
            this.textBox3.TabIndex = 5;
            this.textBox3.Text = "I/O";
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Location = new System.Drawing.Point(446, 3);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(14, 279);
            this.vScrollBar1.TabIndex = 6;
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "CPU0";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "CPU0",
            "CPU1",
            "CPU2",
            "CPU3"});
            this.comboBox1.Location = new System.Drawing.Point(9, 38);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Text = "CPU0";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "AICP_0",
            "AICP_2",
            "VRM",
            "CHP_0",
            "CHP_1",
            "CHP_2"});
            this.comboBox2.Location = new System.Drawing.Point(9, 82);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.Text = "ACPI_0";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "HDD0",
            "HDD1",
            "HDD2",
            "HDD3"});
            this.comboBox3.Location = new System.Drawing.Point(9, 130);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 2;
            this.comboBox3.Text = "HDD0";
            this.comboBox3.SelectedIndexChanged += new System.EventHandler(this.comboBox3_SelectedIndexChanged);
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(147, 38);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(32, 20);
            this.textBox4.TabIndex = 3;
            this.textBox4.Text = "36 C";
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(147, 82);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(32, 20);
            this.textBox5.TabIndex = 4;
            this.textBox5.Text = "54 C";
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(147, 131);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(32, 20);
            this.textBox6.TabIndex = 5;
            this.textBox6.Text = "41 C";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "CPU",
            "Motherboard",
            "Storage"});
            this.listBox1.Location = new System.Drawing.Point(370, 58);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(157, 82);
            this.listBox1.TabIndex = 6;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.cont);
            this.Name = "Form1";
            this.Text = "Form1";
            this.cont.ResumeLayout(false);
            this.Conexiones.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl cont;
        private System.Windows.Forms.TabPage Conexiones;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ToolStripButton toolStripTextBox1;
        private System.Windows.Forms.ToolStripButton toolStripTextBox2;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ProgressBar progressBar3;
        private System.Windows.Forms.ProgressBar progressBar2;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.ListBox listBox1;
    }
}

