import java.util.Scanner;

public class Libreria {

	public static int contarLetras(int opciones, String texto) {
		int ret=0;
		if(opciones==0) {
			ret=0;
		}else {
			for(int i = 0; i < texto.length(); i++) {
				if(opciones%10==1) {
					if(texto.charAt(i)==' ') ret++;
				}
				if(opciones%100>=10) {
					if(texto.charAt(i)>='a'&&texto.charAt(i)<='z') ret++;
				}
				if(opciones%1000>=100) {
					if(texto.charAt(i)>='A'&&texto.charAt(i)<='Z') ret++;
				}
			}
		}
		return ret;
	}
	
	public static int potencia(int base, int exponente) {
		int ret=1;
		for(int i=0;i<exponente;i++) {
			ret*=base;
		}
		return ret;
	}
	
	public static void rellenaVector(int[] vector, int tam) {
		Scanner es=new Scanner(System.in);
		for(int i=0;i<tam;i++) {
			System.out.println("Introduce un valor");
			vector[i]=es.nextInt();
		}
	}
	
	public static void mostrarPar(boolean par, int[] vector) {
		for(int i=0;i<vector.length;i++) {
			if(par) {if(i%2==0)System.out.println(vector[i]);}
			else if(i%2!=0)System.out.println(vector[i]);
		}
	}

}
