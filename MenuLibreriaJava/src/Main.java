import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner es=new Scanner(System.in);
		boolean repetir=true;
		int[] vector=new int[0];
		while(repetir) {
			System.out.println("Elige una opcion:\n1: cuenta letras de una frase\n2: calcula una potencia\n3: rellena un vector\n4: mira el vector\n5: salir");
			int opciones=es.nextInt();
			switch(opciones) {
			case 1:{
				System.out.println("Initroduce una linea de texto");
				es.nextLine();
				String text=es.nextLine();
				System.out.println("Quieres contar los espacios?(s/n)");
				String selecter=es.nextLine();
				int selector=0;
				while(!(selecter.equalsIgnoreCase("s")||selecter.equalsIgnoreCase("n"))) {
					selecter=es.nextLine();
				}
				if(selecter.equalsIgnoreCase("s")) selector+=1;
				System.out.println("Quieres contar las Minusculas?(s/n)");
				selecter=es.nextLine();
				while(!(selecter.equalsIgnoreCase("s")||selecter.equalsIgnoreCase("n"))) {
					selecter=es.nextLine();
				}
				if(selecter.equalsIgnoreCase("s")) selector+=10;
				System.out.println("Quieres contar las Mayusculas?(s/n)");
				selecter=es.nextLine();
				while(!(selecter.equalsIgnoreCase("s")||selecter.equalsIgnoreCase("n"))) {
					selecter=es.nextLine();
				}
				if(selecter.equalsIgnoreCase("s")) selector+=100;
				System.out.println(Libreria.contarLetras(selector, text));
				break;
				}
			case 2:
				System.out.println("Introduce la base");
				int base=es.nextInt();
				System.out.println("Introduce el exponente");
				int exp=es.nextInt();
				System.out.println(Libreria.potencia(base, exp));
				break;
			case 3:
				System.out.println("Introduce el tamanio del vector");
				int tam=es.nextInt();
				while(tam<=0) {
					tam=es.nextInt();
				}
				vector=new int[tam];
				for(int i=0;i<tam;i++) {
					System.out.println("Initroduce un numero para el vector");
					vector[i]=es.nextInt();
				}
				break;
			case 4:
				System.out.println("Quieres ver las posiciones pares o las impares? si quieres las pares introduce 0, u otro numero para los impares");
				Libreria.mostrarPar(es.nextInt()==0, vector);
				break;
			case 5:
				repetir=false;
				break;
			default:
				System.out.println("Opcion invalida");
			}
		}
		System.out.println("Buenos dias");
		
	}

}
