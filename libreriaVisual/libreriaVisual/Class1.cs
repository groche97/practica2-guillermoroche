﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libreriaVisual
{
    public class libreriaVisual
    {
        public static int contarLetras(int opciones, String texto)
        {
            int ret = 0;
            if (opciones == 0)
            {
                ret = 0;
            }
            else
            {
                for (int i = 0; i < texto.Length; i++)
                {
                    if (opciones % 10 == 1)
                    {
                        if (texto.ElementAt(i)==' ') ret++;
                    }
                    if (opciones % 100 == 1)
                    {
                        if (texto.ElementAt(i) >= 'a' && texto.ElementAt(i) <= 'z') ret++;
                    }
                    if (opciones % 1000 == 1)
                    {
                        if (texto.ElementAt(i) >= 'A' && texto.ElementAt(i) <= 'Z') ret++;
                    }
                }
            }
            return ret;
        }

        public static int potencia(int basep, int exponente)
        {
            int ret = 1;
            for (int i = 0; i < exponente; i++)
            {
                ret *= basep;
            }
            return ret;
        }

        public static void rellenaVector(int[] vector, int tam)
        {
            for (int i = 0; i < tam; i++)
            {
                Console.WriteLine("Introduce un valor");
                vector[i] = int.Parse(Console.ReadLine());
            }
        }

        public static void mostrarPar(bool par, int[] vector)
        {
            for (int i = 0; i < vector.Length; i++)
            {
                if (par) { if (i % 2 == 0) Console.WriteLine(vector[i]); }
                else if (i % 2 != 0) Console.WriteLine(vector[i]);
            }
        }

    }
}
